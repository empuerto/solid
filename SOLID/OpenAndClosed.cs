﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLID
{
    class OpenAndClosed
    {
        List<ICountry> countryList = new List<ICountry>();

        public OpenAndClosed()
        {
            
        }

        void populateCountryList()
        {
            countryList = new List<ICountry>();
            countryList.Add(new USA_Country());
            countryList.Add(new UK_Country());
        }

        ICountry getCountryFromString(string p_CountryCode)
        {
            foreach (ICountry i in countryList)
            {
                if (i.countryCode == i.countryCode)
                {
                    return i;
                }
            }
            return null;
        }
    }

    interface ICountry
    {
        string countryCode { get; set; }
        TaxCalculatorBase taxCalculator { get; set; }
    }

    class USA_Country : ICountry
    {
        public TaxCalculatorBase taxCalculator { get; protected set; }
        string countryCode { get; protected set; }

        public USA_Country()
        {
            taxCalculator = new USATax();
            countryCode = "USA";
        }

    }

    class UK_Country : ICountry
    {
        public TaxCalculatorBase taxCalculator { get; protected set; }
        string countryCode { get; protected set; }

        public UK_Country()
        {
            taxCalculator = new UKTax();
            countryCode = "UK";
        }

    }

    abstract class TaxCalculatorBase
    {
        public decimal TotalAmount { get; set; }
        public abstract decimal CalculateTax();
    }

    class USATax : TaxCalculatorBase
    {
        public override decimal CalculateTax()
        {
            //calculate tax as per USA rules
            return 0;
        }
    }

    class UKTax : TaxCalculatorBase
    {
        public override decimal CalculateTax()
        {
            //calculate tax as per UK rules
            return 0;
        }
    }

}
